import * as express from 'express';
import * as next from 'next';
import * as cache from 'lru-cache';
import * as compression from 'compression';
import { join } from 'path';
import { parse } from 'url';

const dev = process.env.NODE_ENV !== 'production';
const port = process.env.PORT ? parseInt(process.env.PORT) : 3000;

const app = next({ dev });
const handle = app.getRequestHandler();

const ssrCache = new cache({
  max: 20,                // not more than 20 results will be cached
  maxAge: 1000 * 60 * 5,  // 5 mins
});

app.prepare().then(() => {
  const server = express();
  
  server.use(compression());
  
  ['/', 'about-us', 'contact-us'].forEach(path => {
    server.get(path, (req, res) => {
      renderAndCache(req, res);
    });
  });
  server.get('/services/:slug', (req, res) => {
    renderAndCache(req, res, { slug: req.params.slug });
  });
  server.get('*', (req, res) => {
    const parsedUrl = parse(req.url, true);
    const { pathname } = parsedUrl;
    if (pathname === '/sw.js') {
console.log(`dev? ${dev}, __dirname: ${__dirname}`);
      app.serveStatic(req, res, join(__dirname, '..', 'static', 'workbox', 'sw.js'))
        .then(() => { console.log(`resolving /sw.js statusCode: ${res.statusCode}`)});
    } else {
      handle(req, res);
    }
  });
  server.listen(port, (err: Error) => {
    if (err) {
      throw err;
    }
    console.log(`🏡 Ready on http://localhost:${port}`);
  });
});
    
async function renderAndCache(
  req: express.Request,
  res: express.Response,
  queryParams: Record<string, string | string[] | undefined> | undefined = req.query
) {
  const key = req.path;

  // if page is in cache, server from cache
  if (ssrCache.has(key)) {
    res.setHeader('x-cache', 'HIT');
    res.send(ssrCache.get(key));
    return;
  }

  try {
    // if not in cache, render the page into HTML
    const html = await app.renderToHTML(req, res, req.path, queryParams);

    // if something wrong with the request, let's skip the cache
    if (res.statusCode !== 200) {
      res.send(html);
      return;
    }

    ssrCache.set(key, html);

    res.setHeader('x-cache', 'MISS');
    res.send(html);
  } catch (err) {
    app.renderError(err, req, res, req.path, queryParams || req.query);
  }
}
