import Head from "next/head";

const AboutUs = () => (
  <div className="container">
    <Head>
      <title>About us | Poliambulatorio Aleman</title>
    </Head>
    <div className="row">
      <div className="col-12-sm">
        <h1>Chi siamo</h1>
        <p className="text-justify mb-4">
          <img className="rounded float-right pl-4" src="/static/images/aleman-indicazioni.jpg" />
          Il <strong>Poliambulatorio Medico Specialistico Aleman</strong> offre
          tecnologie avanzate in grado di fornire una vasta gamma di servizi
          specialistici per la vostra salute e benessere.
          <br />
          E’ nostro obiettivo essere operatori nel campo sanitario al servizio del
          paziente.
          <br />
          Vogliamo fornire prestazioni sanitarie sempre più mirate, evolute e
          aggiornate per effettuare prevenzione, diagnosi e trattamento terapeutico.
          <br/>
          Per garantire costantemente un servizio di qualità facciamo sì che le
          nostre attività siano ispirate a principi di competenza, efficienza,
          cortesia, disponibilità e trasparenza.
        </p>
        Per la sua soddisfazione ci impegniamo a:
        <ul>
          <li>
            essere disponibili nel recepire i suoi bisogni,
          </li>
          <li>
            fornire una risposta concreta alle sue necessità nel minor tempo 
            possibile,
          </li>
          <li>
            mantenere l’onere economico adeguato al servizio fornitochi siamo.
          </li>
        </ul>

        <p className="text-justify">
          <img className="rounded float-left pr-4" src="/static/images/aleman-pieghevoli.jpg" />
          Il Poliambulatorio Aleman è dotato di una reception con annessa sala di
          attesa, dove sono a disposizione depliant esplicativi di tutte le attività
          svolte e di sei camere ambulatoriali, tre con servizi interni. I pazienti
          possono ricevere informazioni e prenotare visite sia di persona che
          telefonicamente.
          <br/>
          E’ garantita la privacy dell’ambiente e dei dati personali. Uniamo, alla
          professionalità dell’intera Equipe Medica che lavora in sinergia, la
          massima flessibilità nel fornire un’ampia gamma di servizi, adeguandoci
          alle diverse esigenze dei pazienti e prestando loro la massima attenzione.
        </p>

        <p>
          Direttore Sanitario
          <br/>
          <strong>Dr. Francesco Laganà</strong>
        </p>
      </div>
    </div>
  </div>
)

export default AboutUs;
