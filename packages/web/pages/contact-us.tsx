import { Breadcrumbs } from '../components/breadcrumbs/breadcrumbs.component';
import { MapContainer } from '../components/map/map-container.component';
import { ContactForm } from '../components/contact-form/contact-form.component';
import { ErrorBoundary } from '../components/error-boundary';
import Head from 'next/head';

const ContactUs = () => (
  <>
    <Breadcrumbs />
    <div className="container">
      <Head>
        <title>Contatti | Poliambulatorio Aleman</title>
      </Head>
      <section className="bar">
        <div className="row">
          <div className="col-md-12">
            <div className="heading">
              <h2>Siamo qui per aiutarti</h2>
            </div>
            <p className="lead">
              Are you curious about something? Do you have some kind of problem with
              our products? As am hastily invited settled at limited civilly fortune
              me. Really spring in extent an by. Judge but built gay party world. Of
              so am he remember although required. Bachelor unpacked be advanced at.
              Confined in declared marianne is vicinity.
            </p>
            <p className="text-sm">
              Please feel free to contact us, our customer service center is working
              for you 24/7.
            </p>
          </div>
        </div>
      </section>
      <section>
        <div className="row text-center">
          <div className="col-md-4">
            <div className="box-simple">
              <div className="icon-outlined">
                <i className="fa fa-map-marker" />
              </div>
              <h3 className="h4">Indirizzo</h3>
              <p>
                Via Peppino Ribolla, 1<br />
                24022 Alzano Lombardo (BG)<br />
              </p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="box-simple">
              <div className="icon-outlined">
                <i className="fa fa-phone" />
              </div>
              <h3 className="h4">Call center</h3>
              <p>
                Puoi contattarci telefonicamente o inviarci un fax al seguente numero
              </p>
              <p>
                <strong><a href="tel:035510563">035 510563</a></strong>
              </p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="box-simple">
              <div className="icon-outlined">
                <i className="fa fa-envelope" />
              </div>
              <h3 className="h4">Supporto elettronico</h3>
              <p>
                Sentiti libero di inviarci un'email per avere risposta alle tue 
                domande.
              </p>
              <ul className="list-unstyled text-sm">
                <li>
                  <strong>
                    <a href="mailto:info@poliambulatorioaleman.com">
                      info@poliambulatorioaleman.com
                    </a>
                  </strong>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section className="bar pt-0">
        <div className="row">
          <div className="col-md-12">
            <div className="heading text-center">
              <h2>Modulo richiesta informazioni</h2>
            </div>
          </div>
          <div className="col-md-8 mx-auto">
            <ErrorBoundary>
              <ContactForm />
            </ErrorBoundary>
          </div>
        </div>
      </section>
    </div>
    <MapContainer />
  </>
);

export default ContactUs;
