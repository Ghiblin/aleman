import App, { Container, NextAppContext, DefaultAppIProps, AppProps } from 'next/app';
import ApolloClient from 'apollo-client';
import { NormalizedCacheObject } from 'apollo-cache-inmemory';
import { ApolloProvider, getDataFromTree } from 'react-apollo';
import { Layout } from '../components/layout.component';
import { getUserFromLocalCookie, getUserFromServerCookie } from '../lib/auth';
import Router from 'next/router';

import getConfig from 'next/config';
import { HttpLink } from 'apollo-link-http';
import { ApolloConfig, initApollo } from '../lib/initApollo';
import Head from 'next/head';
import { OfflineSupport } from '../components/offline-support';

const {
  publicRuntimeConfig: {
    API_URL
  }
} = getConfig();

const apolloConfig: ApolloConfig = {
  link: new HttpLink({
    uri: `${API_URL}graphql`,
  }),
};

interface Props {
  apollo: ApolloClient<NormalizedCacheObject>;
  currentUser: string;
  isAuthenticated: boolean;
  serverState: {
    apollo: any;
  }
}

class AlemanApp extends App<Props> {
  static async getInitialProps({ Component, ctx }: NextAppContext) {
    const currentUser = process.browser
        ? getUserFromLocalCookie()
        : getUserFromServerCookie(ctx.req!);

    let serverState = { apollo: {} };

    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    // Run all GraphQL queries in the component tree
    // and extract the resulting data
    if (!process.browser) {
      const apollo = initApollo(apolloConfig, null, ctx);

      // Provide the `url` prop data in case a GraphQL query uses it
      const url = {
        query: ctx.query,
        pathname: ctx.pathname,
      };

      try {
        // Run all GraphQL queries
        await getDataFromTree(
          <ApolloProvider client={apollo}>
            <Component
              url={url}
              ctx={ctx}
              {...pageProps}
            />
          </ApolloProvider>,
          {
            router: {
              asPath: ctx.asPath,
              pathname: ctx.pathname,
              query: ctx.query,
            }
          }
        );
      } catch (error) {
        // Prevent Apollo Client GraphQL errors from crashing SSR.
        // Handle them in components via the data.error prop:
        // http://dev.apollodata.com/react/api-queries.html#graphql-query-data-error
        console.log(error);
      }
      // getDataFromTree does not call componentWillUnmount
      // head side effect therefore need to be cleared manually
      Head.rewind();

      // Extract query data from the Apollo store
      serverState = {
        apollo: {
          data: apollo.cache.extract(),
        },
      };
    }

    return {
      pageProps,
      loggedUser: currentUser || 'guest',
      isAuthenticated: !!currentUser,
      serverState,
    };
  }

  readonly apollo: ApolloClient<NormalizedCacheObject>;

  constructor(props: Readonly<Props & DefaultAppIProps & AppProps<Record<string, string | string[] | undefined>>>) {
    super(props);
    this.apollo = initApollo(
      apolloConfig,
      this.props.serverState.apollo.data,
    );
  }

  public render() {
    const { Component, pageProps, isAuthenticated, currentUser } = this.props;
    return (
      <Container>
        <OfflineSupport />
        <ApolloProvider client={this.apollo}>
          <Layout isAuthenticated={isAuthenticated} currentUser={currentUser}>
            <Component {...pageProps} />
          </Layout>
        </ApolloProvider>
      </Container>
    );
  }

  private logout = (evt: StorageEvent) => {
    if (evt.key === 'logout') {
      Router.push(`/?logout=${evt.newValue}`);
    }
  }

  public componentDidMount() {
    window.addEventListener('storage', this.logout, false);
  }

  public componentWillUnmount() {
    window.removeEventListener('storage', this.logout, false);
  }
}

// export default withApollo(AlemanApp);
export default AlemanApp;
