import { NextFunctionComponent, NextContext } from "next";
import { Service } from "../components/services/service.component";

interface Props {
  slug: string | string[];
}

const Services: NextFunctionComponent<Props> = ({ slug }) => (
  <section className="bar background-white relative-positioned">
    <div className="container">
      <Service slug={typeof slug === 'string' ? slug : slug[0]} />
    </div>
  </section>
);

Services.getInitialProps = async ({ query: { slug = '' }, res }: NextContext) => {
  if (!slug && res) {
    res.statusCode = 404;
  }
  
  return { 
    slug,
  };
}

export default Services;
