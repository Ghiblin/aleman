import Head from 'next/head';
import { HomeCarousel } from "../components/home-carousel/home-carousel.component";

const Index = () => {
  return (
    <>
      <Head>
        <title>Poliambulatorio Aleman</title>
      </Head>
      <section className="bar background-white relative-positioned">
        <div className="container">
          <HomeCarousel interval={30} />
        </div>
      </section>
    </>
  );
}

export default Index;
