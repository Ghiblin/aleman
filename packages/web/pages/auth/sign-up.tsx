import React, { Component } from 'react';
import { RegisterForm } from '../../components/auth/register-form.component';
import { LoginForm } from '../../components/auth/login-form.component';

export default class SignUp extends Component<{}, {}> {
  public render() {
    console.log(`SignUp page render props:${JSON.stringify(this.props)}`);
    return (
      <div id="content">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="box">
                <h2 className="text-uppercase">New Account</h2>
                <p className="lead">Not our registered customer yet?</p>
                <RegisterForm />
                <hr />
              </div>
            </div>
            <div className="col-lg-6">
              <div className="box">
                <h2 className="text-uppercase">Login</h2>
                <p className="lead">Already our customer?</p>
                <hr />
                <LoginForm />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
