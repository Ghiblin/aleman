const withPlugins = require('next-compose-plugins');
const withTypescript = require('@zeit/next-typescript');
const withSass = require('@zeit/next-sass');

const NextWorkboxPlugin = require('next-workbox-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const path = require('path');

const isDev = process.env.NODE_ENV !== 'production';

module.exports = withPlugins(
  [
    withSass,
    withTypescript,
  ],
  {
    publicRuntimeConfig: {
      isDev,
      API_URL: `http://localhost:1337/`,
    },
    webpack(config, { isServer, buildId, dev }) {
      const { plugins = [] } = config;
      const otherPlugins = [];
      config.node = {
        fs: 'empty',
      };

      const workboxOptions = {
        clientsClaim: true,
        skipWaiting: true,
        globPatterns: [
          '.next/static/*', 
          '.next/static/commons/*',
        ],
        modifyUrlPrefix: {
          '.next': '/_next',
        },
        runtimeCaching: [
          {
            urlPattern: /.*\.(?:css|png|jpg|jpeg|svg|gif)/,
            handler: 'cacheFirst',
            options: {
              cacheName: 'image-cache',
              cacheableResponse: {
                statuses: [0, 200],
              },
            },
          },
          {
            urlPattern: /on-demand-entries-ping/,
            handler: 'networkOnly'
          },
          {
            urlPattern: /\/api\//,
            handler: 'networkFirst',
            options: {
              cacheName: 'cache-with-expiration',
              expiration: {
                maxEntries: 20,
                maxAgeSeconds: dev ? 5 : 60
              },
              cacheableResponse: {
                statuses: [0, 200]
              },
              plugins: []
            }
          },
          {
            urlPattern: /(localhost|YOUR\.DOMAIN)/i,
            handler: 'staleWhileRevalidate',
            options: {
              cacheableResponse: {
                statuses: [0, 200]
              },
              plugins: []
            }
          }
        ],
      };
      // if (!isServer && !dev) {
      if (!isServer) {
        otherPlugins.push(
          new NextWorkboxPlugin({
            buildId,
            ...workboxOptions,
          }),
          new WebpackPwaManifest({
            filename: 'static/manifest.json',
            name: 'Poliambulatorio Aleman',
            short_name: 'Aleman',
            description: 'Sito web del poliambulatorio Aleman',
            background_color: '#ffffff',
            theme_color: '#5755d9',
            display: 'standalone',
            orientation: 'portrait',
            fingerprints: false,
            inject: false,
            start_url: '/',
            ios: {
              'apple-mobile-web-app-title': 'Aleman',
              'apple-mobile-web-app-status-bar-style': '#5755d9',
            },
            icons: [
              {
                src: path.resolve('static/favicon.ico'),
                sizes: [96, 128, 192, 256, 384, 512],
                destination: '/static',
              },
            ],
            includeDirectory: true,
            publicPath: '..',
          }),
        );
      }

      return {
        ...config,
        stats: {
          colors: true,
          reasons: true,
          hash: true,
          version: true,
          timings: true,
          chunks: true,
          chunkModules: true,
          cached: true,
          cachedAssets: true
        },
        plugins: [
          ...plugins,
          ...otherPlugins,
        ],
      };
    },
  },
);
