import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';

export const MapContainer = GoogleApiWrapper({
  apiKey: 'AIzaSyCKbjFAcwgSDePzfC5hzlYh3_70hT9ReZw',
  language: 'it-IT',
})(({ google }) => (
  <div id="map" style={{ position: 'relative', overflow: 'hidden' }}>
    <Map
      google={google}
      zoom={14}
      initialCenter={{
        lat: 45.731540, 
        lng:  9.728839,
      }}
      draggable={true}
      disableDefaultUI
    >
      <Marker />
    </Map>
  </div>
))