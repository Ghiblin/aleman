import React from 'react';
import { NextContext, NextComponentType } from 'next';
import Router from 'next/router';
import { getUserFromLocalCookie, getUserFromServerCookie } from '../../lib/auth';

interface DefaultPageProps {
  loggedUser?: string;
  currentUrl: string;
  isAuthenticated: boolean;
}

export const withDefaultPage = <P extends object>(Page: NextComponentType<P>) =>
  class DefaultPage extends React.Component<P & DefaultPageProps> {
    static async getInitialProps(ctx: NextContext) {
      const loggedUser = process.browser
        ? getUserFromLocalCookie()
        : getUserFromServerCookie(ctx.req!);
      const pageProps = Page.getInitialProps && Page.getInitialProps(ctx);

      const currentUrl = ctx.req ? ctx.req.url : '';
      return {
        ...pageProps,
        loggedUser,
        currentUrl,
        isAuthenticated: !!loggedUser,
      }
    }

    private logout = (evt: StorageEvent) => {
      if (evt.key === 'logout') {
        Router.push(`/?logout=${evt.newValue}`);
      }
    }

    public componentDidMount() {
      window.addEventListener('storage', this.logout, false);
    }

    public componentWillUnmount() {
      window.removeEventListener('storage', this.logout, false);
    }

    public render() {
      return (
        <Page {...this.props} />
      );
    }
  };