import React from 'react';
import { NextComponentType, NextContext } from 'next';
import { withDefaultPage } from './defaultPage';

interface SecurePageProps {
  isAuthenticated: boolean;
}

const withSecurePage = (Page: NextComponentType) => 
  class SecurePage extends React.Component<SecurePageProps> {
    public static async getInitialProps(ctx: NextContext) {
      let pageProps = {};
      if (Page.getInitialProps) {
        pageProps = await Page.getInitialProps(ctx);
      }

      return {
        isAuthenticated: false,
        pageProps,
      };
    }

    public render() {
      const { isAuthenticated, ...props } = this.props;
      return isAuthenticated
        ? <Page {...props} />
        : "Not Authorized";
    }
  }

export default (Page: NextComponentType) => withDefaultPage(withSecurePage(Page));
