import gql from 'graphql-tag';

export const GET_SERVICE = gql`
query getService($slug: String) {
  services(where: { slug: $slug }) {
    _id
    title
    name
    slug
    description
  }
}
`

interface ServiceItem {
  _id: string;
  title: string;
  name: string;
  slug: string;
  description: string;
}
export interface GetServiceData {
  services: ServiceItem[];
}

export interface GetServiceVariables {
  slug: string;
}
