import { FC } from "react";
import { Query } from "react-apollo";
import * as Markdown from 'react-markdown';
import { GetServiceData, GetServiceVariables, GET_SERVICE } from "./get-service.query";
import Error from 'next/error';
import Head from "next/head";

interface Props {
  slug: string;
}

export const Service: FC<Props> = ({ slug }) => (
  <Query<GetServiceData, GetServiceVariables> query={GET_SERVICE} variables={{ slug }}>
    {({ data, loading, error}) => {
      if (loading) {
        return (<div>Loading...</div>);
      }
      if (error) {
        return <Error statusCode={500} />
      }
      if (!data || data.services.length === 0) {
        return <Error statusCode={404} />
      }
      const service = data.services[0];
      return (
        <>
          <Head>
            <title>{service.title} | Poliambulatorio Aleman</title>
          </Head>
          <h1>{ service.title }</h1>
          <Markdown source={service.description} escapeHtml={false} />
        </>
      )
    }}
  </Query>
);
