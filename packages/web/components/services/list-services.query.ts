import gql from 'graphql-tag';

export const LIST_SERVICES = gql`
  query {
    services(sort: "name:asc") {
      name
      title
      slug
    }
  }
`;

interface ServiceItem {
  name: string;
  title: string;
  slug: string;
}

export interface ListServicesData {
  services: ServiceItem[];
}
