import React, { useState } from 'react';
import Link from 'next/link';
import { unsetToken } from '../../lib/auth';

import { LoginForm } from '../auth/login-form.component';
import Router from 'next/router';
import { Modal } from '../bootstrap/modal';

interface TopBarProps {
  isAuthenticated: boolean;
}

export const TopBar: React.SFC<TopBarProps> = ({ isAuthenticated }) => {
  const [showLogin, setShowLogin] = useState(false);
  const openLogin = () => setShowLogin(true);
  const closeLogin = () => {
    setShowLogin(false);
  }
  const onSignupClick = () => {
    closeLogin();
    Router.push('/auth/sign-up');
  }

  return (
    <div className="top-bar">
      <div className="container">
        <div className="row d-flex align-items-center">
          <div className="col-md-6 d-md-block d-none">
            <p>Contattaci al 035 510563 o info@poliambulatorioaleman.com</p>
          </div>
          <div className="col-md-6">
            <div className="d-flex justify-content-md-end justify-content-between">
              <ul className="list-inline contact-info d-block d-md-none">
                <li className="list-inline-item">
                  <a href="tel:035510563"><i className="fas fa-phone" /></a>
                </li>
                <li className="list-inline-item">
                  <a href="mail:info@poliambulatorioaleman.com"><i className="fas fa-envelope" /></a>
                </li>
              </ul>
              <div className="login">
                {
                  isAuthenticated
                    ? <a className="logout-btn" onClick={unsetToken}>
                        <i className="fas fa-sign-out-alt" />
                        <span className="d-none d-md-inline-block">Log out</span>
                      </a>
                    : <>
                        <a className="login-btn" onClick={openLogin}> 
                          <i className="fas fa-sign-in-alt" />
                          <span className="d-none d-md-inline-block">Sign In</span>
                        </a>
                        <Modal isOpen={showLogin} onClose={closeLogin}>
                          {{
                            header: 'Customer Login',
                            body:
                              <> 
                                <LoginForm callback={closeLogin} compact />
                                <p className="text-center text-muted">Not registered yet?</p>
                                <p className="text-center text-muted">
                                  <a onClick={onSignupClick} href="/auth/sign-up">
                                    <strong>Register now</strong>
                                  </a>!
                                  It is easy and done in 1&nbsp;minute and gives you access to special discounts and much more!
                                </p>
                              </> 
                          }}
                        </Modal>
                        <Link href="/auth/sign-up">
                          <a className="signup-btn">
                            <i className="fas fa-user" />
                            <span className="d-none d-md-inline-block">Sign Up</span>
                          </a>
                        </Link>
                      </>
                }
              </div>
              <ul className="social-custom list-inline">
                <li className="list-inline-item">
                  <a href="#"><i className="fab fa-facebook-f" /></a>
                </li>
                <li className="list-inline-item">
                  <a href="#"><i className="fab fa-google-plus-g" /></a>
                </li>
                <li className="list-inline-item">
                  <a href="#"><i className="fab fa-twitter" /></a>
                </li>
                <li className="list-inline-item">
                  <a href="#"><i className="fas fa-envelope" /></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}