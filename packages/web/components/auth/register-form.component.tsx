import { SFC } from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import { strapiRegister } from '../../lib/auth';
import { Input } from '../bootstrap/input';

interface RegisterData {
  username?: string;
  email?: string;
  password?: string;
}

const initialValues: RegisterData = {
  username: '',
  email: '',
  password: '',
}

export const RegisterForm: SFC<{}> = () => (
  <Formik<RegisterData>
    onSubmit={async (values, {}) => {
      const { username, password, email } = values;
      const auth = await strapiRegister(username!, email!, password!);
      console.log('auth', auth);
    }}
    initialValues={initialValues}
    validationSchema={Yup.object().shape({
      username: Yup.string().required(),
      email: Yup.string().email().required(),
      password: Yup.string().required(),
    })}
    render={() => (
      <Form>
        <Field
          type="text"
          name="username"
          label="Username"
          component={Input}
        />
        <Field
          type="email"
          name="email"
          label="Email"
          component={Input}
        />
        <Field
          type="password"
          name="password"
          label="Password"
          component={Input}
        />
        <div className="text-center">
          <button type="submit" className="btn btn-template-outlined">
            <i className="fa fa-user-md" /> Register
          </button>
        </div>
      </Form>
    )}
  />
);
