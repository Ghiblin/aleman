import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import { strapiLogin } from '../../lib/auth';
import { Input } from '../bootstrap/input';
import { createPropsGetter } from '../bootstrap/utils';

type Props = {
  callback?: () => void;
} & Partial<DefaultProps>;

type DefaultProps = Readonly<typeof defaultProps>;

type LoginData = Readonly<typeof initialValues>;

const initialValues = Object.freeze({
  email: '',
  password: '',
});

const defaultProps = Object.freeze({
  compact: false,
});

const getProps = createPropsGetter(defaultProps);

export const LoginForm = (props: Props) => {
  const { callback, compact } = getProps(props);

  return (
    <Formik<LoginData>
      onSubmit={async (values) => {
        const { email, password } = values;
        try {
          await strapiLogin(email!, password!);
          callback && callback();
        } catch (ex) {
          console.error(`failed to login`, ex.message);
        }
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string().email().required(),
        password: Yup.string().required(),
      })}
      initialValues={initialValues}
      render={() => (
        <Form>
          <Field
            type="email"
            name="email"
            label={!compact && 'Email'}
            component={Input}
          />
          <Field
            type="password"
            name="password"
            label={!compact && 'Password'}
            component={Input}
          />
          <div className="text-center">
            <button type="submit" className="btn btn-template-outlined">
              <i className="fa fa-sign-in" /> Log in
            </button>
          </div>
        </Form>
      )}
    />
  );
}