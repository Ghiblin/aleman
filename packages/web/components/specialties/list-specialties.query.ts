import gql from "graphql-tag";

export const LIST_SPECIALTIES = gql`
  query {
    specialties(sort: "name:asc") {
      name
      doctors {
        prefix
        firstName
        lastName
      }
    }
  }
`;

interface DoctoryItem {
  prefix?: string;
  firstName: string;
  lastName: string;
}

export interface SpecialtyItem {
  name: string;
  doctors: DoctoryItem[];
}