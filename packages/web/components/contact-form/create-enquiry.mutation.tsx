  import gql from 'graphql-tag';

export const CREATE_ENQUIRY = gql`
  mutation createEnquiry($details: EnquiryInput) {
    createEnquiry (
      input: {
        data: $details
      }
    ) {
      enquiry {
        _id
        firstName
        lastName
        email
        subject
        message
      }
    }
  }
`;

export interface CreateEnquiryData {
  enquiry: {
    _id: string;
    firstName?: string;
    lastName?: string;
    email: string;
    subject: string;
    message: string;
  }
}

export interface CreateEnquiryVariables {
  details: CreateEnquiryInput;
}

export interface CreateEnquiryInput {
  firstName?: string;
  lastName?: string;
  email: string;
  subject?: string;
  message: string;
}
