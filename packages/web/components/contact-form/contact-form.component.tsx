import { FC, useState } from 'react';
import { Mutation } from 'react-apollo';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import { CREATE_ENQUIRY, CreateEnquiryData, CreateEnquiryVariables } from './create-enquiry.mutation';
import { Alert } from '../bootstrap/alert';
import { Input } from '../bootstrap/input';
import { Textarea } from '../bootstrap/textarea';

interface ContactFormProps {
  firstName?: string;
  lastName?: string;
  email?: string;
  subject?: string;
  message?: string;
}

const initialValues: ContactFormProps = {
  firstName: '',
  lastName: '',
  email: '',
  subject: '',
  message: '',
};

export const ContactForm: FC = () => {
  const [success, setSuccess] = useState(false);

  return (
    <Mutation<CreateEnquiryData, CreateEnquiryVariables> mutation={CREATE_ENQUIRY}>
      {(createEnquiry, {loading, error}) => (
        <>
          <Formik<ContactFormProps>
            initialValues={initialValues}
            onSubmit={ async (values, { resetForm }) => {
              try {
                await createEnquiry({
                  variables: {
                    details: {
                      firstName: values.firstName,
                      lastName: values.lastName,
                      email: values.email!,
                      subject: values.subject,
                      message: values.message!,
                    }
                  }
                });
                resetForm(initialValues);
                setSuccess(true);
                setTimeout(function() { setSuccess(false); }, 3000);
              } catch (ex) {
                console.error('failed to contact server...', ex);
              }
            }}
            validationSchema={Yup.object().shape({
              firstName: Yup.string().max(25),
              lastName: Yup.string().max(25),
              email: Yup.string().email().required(),
              subject: Yup.string().required().max(150),
              message: Yup.string().required().min(50).max(500),
            })}
            render={() => (
              <Form>
                <div className="row">
                  <div className="col-sm-6">
                    <Field
                      type="text"
                      name="firstName"
                      label="Nome"
                      component={Input}
                    />
                  </div>
                  <div className="col-sm-6">
                    <Field
                      type="text"
                      name="lastName"
                      label="Cognome"
                      component={Input}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <Field 
                      type="email"
                      name="email"
                      label="Email"
                      component={Input}
                    />
                  </div>
                  <div className="col-sm-6">
                    <Field 
                      type="text"
                      name="subject"
                      label="Oggetto"
                      component={Input}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12">
                    <Field
                      type="textarea"
                      name="message"
                      label="Messaggio"
                      component={Textarea}
                    />
                  </div>
                </div>
                {
                  success && (
                    <div className="row">
                      <div className="col-sm-12">
                        <Alert color="success" dismissable fade>
                          <div className="alert-heading">
                            Grazie per averci contattato. 
                          </div>
                          <p>
                            Ti risponderemo appena possibile.
                          </p>
                        </Alert>
                      </div>
                    </div>
                  )
                }
                <div className="row">
                  <div className="col-sm-12 text-center">
                    <button type="submit" className="btn btn-template-outlined" disabled={loading}>
                      <i className="fa fa-envelope-o" /> Invia messaggio
                    </button>
                    { error && error.message && <span className="invalid-feedback">{error.message}</span> }
                  </div>
                </div>
              </Form>
            )}
            />
        </>
      )}
    </Mutation>
  );
}
