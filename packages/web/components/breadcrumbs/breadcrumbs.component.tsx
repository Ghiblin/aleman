import React from 'react';

import { withRouter } from 'next/router';
import Link from 'next/link';

import pages, { BreadcrumbsPages } from './breadcrumbs.pages';

export const Breadcrumbs = withRouter<{}>(({ router }) => {
  const names = breadcrumbsNames(router!.pathname.slice(1).split('/'), pages).filter((n) => n);
  return (
    <div id="heading-breadcrumbs" className="background-pentagon">
      <div className="container">
        <div className="row d-flex align-items-center flex-wrap">
          <div className="col-md-7">
            <h1 className="h2">{names[names.length - 1].label}</h1>
          </div>
          <div className="col-md-5">
            <ul className="breadcrumb d-flex justify-content-end">
              {
                names.map((name) => (
                  <li key={name.label} className={`breadcrumb-item ${name.active ? 'active' : ''}`}>
                    {name.href ? <Link href={name.href}><a>{name.label}</a></Link> : name.label}
                  </li>
                ))
              }
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
});

interface BreadcrumbsName {
  label: string;
  href?: string;
  active: boolean;
}

function breadcrumbsNames(tokens: string[] = [], currentPages: BreadcrumbsPages = {}): BreadcrumbsName[] {
  if (!tokens.length) { return []; }
  const token = tokens[0];
  const page = currentPages[token] || { label: token };
  const label = typeof page === 'string' ? page : page.label;
  let routes: BreadcrumbsPages = {};
  if (typeof page === 'object') {
    routes = page.routes || {};
  }
  const name: BreadcrumbsName = {
    label,
    href: typeof page === 'object' ? page.href : undefined,
    active: tokens.length === 1,
  };
  return [name].concat(breadcrumbsNames(tokens.slice(1), routes));
}
