export interface BreadcrumbsPage {
  label: string;
  href?: string;
  routes?: BreadcrumbsPages;
}

export interface BreadcrumbsPages {
  [key: string]: string | BreadcrumbsPage;
}

const pages: BreadcrumbsPages = {
  '/': {
    label: 'Home',
    routes: {
      'contact-us': 'Contatti',
    }
  },
  'auth': {
    label: 'Authentication',
    routes: {
        'sign-up': 'New Account / Sign In',
      },
  },
};

export default pages;
