import { Query } from "react-apollo";
import Link from "next/link";

import { LIST_SERVICES, ListServicesData } from "../services/list-services.query";
import { Megamenu } from "../bootstrap/megamenu";

type Props = {
  className?: string;
}

export const HeaderServices = ({ className }: Props) => (
  <Query<ListServicesData> query={LIST_SERVICES}>
    {({ data }) => (
      <Megamenu text="Servizi" 
                imageURL="/static/images/idrocolonterapia.jpg"
                className={className}
      >
        {
          (data && data.services || []).map((service) => (
            <Link
              key={service.slug}
              href={{
                pathname: '/services',
                query: { slug: service.slug },
              }}
              as={`/services/${service.slug}`}
              passHref
              prefetch
            >
              <a className="nav-link">{service.title}</a>
            </Link>
          ))
        }
      </Megamenu>
    )}
  </Query>
);
