import React, { useState, useEffect } from 'react';
import cn from 'classnames';
import Link from 'next/link';
import { withRouter } from 'next/router';
import { Collapse } from '../bootstrap/collapse';
import { HeaderServices } from './header-services.component';

export const Header = withRouter<{}>(({ router }) => {
  const [top, setTop] = useState(-1);
  const [scroll, setScroll] = useState(0);
  const pathname = router ? router.pathname : '/';

  useEffect(() => {
    function handleWindowScroll() {
      setScroll(window.scrollY);
    }
    if (top <= 0) {
      const el = document.querySelector<HTMLElement>('.nav-holder');
      el && setTop(el.offsetTop);
    }
    window.addEventListener('scroll', handleWindowScroll);
    return () => {
      window.removeEventListener('scroll', handleWindowScroll);
    }
  }, [top]);
  return (
    <header className={cn('nav-holder', 'make-stiky', { 'sticky': scroll > top })}>
      <div id="navbar" className="navbar navbar-expand-lg" role="navigation">
        <div className="container">
          <Link href="/" passHref>
            <a className="navbar-brand home">
              <img className="d-none d-md-inline-block" src="/static/images/logoaleman.png" alt="Aleman logo" />
              <img className="d-inline-block d-md-none" src="/static/images/logoaleman-small.png" alt="Aleman logo" />
              <span className="sr-only">Aleman</span>
            </a>
          </Link>
          <Collapse className="btn-template-outlined collapsed" nav>
            <ul className="nav navbar-nav ml-auto">
              <li className={`nav-item ${pathname === '/' ? 'active' : ''}`}>
                <Link href="/" prefetch passHref>
                  <a>Home</a>
                </Link>
              </li>
              <li className={`nav-item ${pathname === 'about-us' ? 'active' : ''}`}>
                <Link href="/about-us" prefetch passHref>
                  <a>Chi siamo</a>
                </Link>
                </li>
              <HeaderServices className={pathname.startsWith('/services') ? 'active' : ''} />
              <li className={`nav-item ${pathname === '/contact-us' ? 'active' : ''}`}>
                <Link href="/contact-us" prefetch passHref>
                    <a>Contatti</a>
                </Link>
              </li>
            </ul>
          </Collapse>
        </div>
      </div>
    </header>
  );
});
