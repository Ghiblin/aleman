import gql from 'graphql-tag';

export const LIST_SPECILISTS_BY_SPECIALTY = gql`
  query {
    specialties(sort: "name:asc") {
      name
      specialists {
        prefix
        firstName
        lastName    
      }
    }
  }
`;

export interface ListSpecialistsBySpecialtyItem {
  prefix?: string;
  firstName: string;
  lastName: string;
}

export interface ListSpecialistsBySpecialtyData {
  specialists: ListSpecialistsBySpecialtyItem[];
}
