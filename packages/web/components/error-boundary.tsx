import { Component } from 'react';

interface ErrorBoundaryState {
  hasError: boolean;
}

export class ErrorBoundary extends Component<{}, ErrorBoundaryState> {
  state = {
    hasError: false,
  }

  static getDerivedStateFromError(error: Error) {
    console.log('getDerivedStateFromError:', error);

    // Update state so the next render will show the fallback UI.
    return { 
      hasError: true,
    };
  }

  public componentDidCatch(error: any, info: any) {
    console.log('componentDidCatch', error, info);
  }

  public render() {
    if (this.state.hasError) {
      return (
        <h1>Ops.... something went wrong</h1>
      );
    }
    return this.props.children;
  }
}
