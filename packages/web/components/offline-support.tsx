import { useEffect } from 'react';

export const OfflineSupport = () => {
  useEffect(() => {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker
        .register('/sw.js')
        .then(() => console.log('service worker registered.'))
        .catch((err: Error) => console.dir(err));
    }
  });

  return null;
}
