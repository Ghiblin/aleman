import { ComponentType, ReactNode } from "react";

export type Color = 
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'light'
  | 'dark';

// NB: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input ne lista altri
export type InputType = 
  | 'date'
  | 'file'
  | 'email'
  | 'password'
  | 'tel'
  | 'text';

export const withDefaultProps = <P extends object, DP extends Partial<P> = Partial<P>>(
  defaultProps: DP,
  Cmp: ComponentType<P>
) => {
  type PropsExcludingDefaults = Pick<P, Exclude<keyof P, keyof DP>>;
  type RecomposedProps = Partial<DP> & PropsExcludingDefaults;
  Cmp.defaultProps = defaultProps;
  return (Cmp as ComponentType<any>) as ComponentType<RecomposedProps>;
}

export const createPropsGetter = <DP extends object>(_defaultProps: DP) => {
  return <P extends Partial<DP>>(props: P) => {
    // we are extratting default props from component Props api type
    type PropsExcludingDefaults = Pick<P, Exclude<keyof P, keyof DP>>;

    // we are re-creating our props definition by creating an intersection type
    // between Props without Defaults and NonNullable DefaultProps
    type RecomposedProps = DP & PropsExcludingDefaults;

    // we are returning the same props that we got as argument - identity function.
    // Also we are turning off compiler and and casting the type to our new 
    // RecomposedProps type
    return (props as any) as RecomposedProps;
  }
}

export const isObject = <T extends object>(value: any): value is T =>
  typeof value === 'object' && typeof value !== 'function' && value != undefined;

type HasRenderProp<T> = T extends { render: (props: any) => ReactNode } ? T : never;
type HasChildrenProp<T> = T extends { children: (props: any) => ReactNode } ? T : never;
type IsFunction<T> = T extends (...args: any[]) => any ? T : never;

export const hasRender = <T extends {}>(value: T): value is HasRenderProp<T> =>
  'render' in value && isFunction((value as HasRenderProp<T>).render);

export const hasChildren = <T extends {}>(value: T): value is HasChildrenProp<T> =>
  'children' in value && isFunction((value as HasChildrenProp<T>).children);

export const isFunction = <T extends {}>(value: T): value is IsFunction<T> =>
  typeof value === 'function';

export const keyCodes = {
  esc:   27,
  space: 32,
  enter: 13,
  tab:   9,
  up:    38,
  down:  40,
  home:  36,
  end:   35,
  n:     78,
  p:     80,
};