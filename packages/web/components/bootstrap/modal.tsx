import React, { useEffect, ReactNode } from 'react';
import cn from 'classnames';
import { Portal } from '../portal';
import { CSSTransition } from 'react-transition-group';
import { isObject, createPropsGetter } from './utils';

type Props = {
  children: ReactNode | NamedChildrenSlots;
  isOpen: boolean;
  onClose: () => void;
} & Partial<DefaultProps>;

type DefaultProps = Readonly<typeof defaultProps>;

type NamedChildrenSlots = {
  header?: ReactNode;
  body: ReactNode;
  footer?: ReactNode;
}

const defaultProps = Object.freeze({
  scrollable: false,
  centered: false,
});
const getProps = createPropsGetter(defaultProps);

const isNamedSlots = (children: any): children is NamedChildrenSlots =>
  isObject(children) && 'body' in children;

export const Modal = (props: Props) => {
  const { isOpen, onClose, children, scrollable, centered} = getProps(props);
  useEffect(() => {
    document.body.classList[isOpen ? 'add' : 'remove']('modal-open');
  });

  function handleOnEnter(node: HTMLElement) {
    node.style.display = 'block';
  }
 
  function handleOnExited(node: HTMLElement) {
    node.style.display = '';
  }

  // function handleKeyPress(evt: KeyboardEvent<HTMLDivElement>) {
  //   if (evt.which === 27) {
  //     onClose();
  //   }
  // }
 
  function renderHeader(header: ReactNode) {
    return (
      <div className="modal-header">
        <h5 className="modal-title">{header}</h5>
        <button type="button" className="close" aria-label="Close" onClick={onClose}>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    );
  }
 
  function renderFooter(footer: ReactNode) {
    return (
      <div className="modal-footer">{footer}</div>
    )
  }
 
  function renderContent(children: ReactNode | NamedChildrenSlots) {
    // run time check for children assertion
    if (!children) {
      throw new Error('children is mandatory!');
    }
 
    // if it's named slots....
    if (isNamedSlots(children)) {
      const { header, body, footer } = children;
      return (
        <div className="modal-content">
          { header && renderHeader(header) }
          <div className="modal-body">{body}</div>
          { footer && renderFooter(footer) }
        </div>
      )
    }
 
    // otherwise it's just a normal children
    return (
      <div className="modal-content">{children}</div>
    )
  }
 
  const className = cn('modal', 'fade', {
    'modal-dialog-scrollable': scrollable,
    'modal-dialog-centered': centered,
  })
 
  return (
    <Portal selector="body">
      <CSSTransition 
        in={isOpen}
        timeout={300}
        classNames={{
          appearActive: 'show',
          enterDone: 'show',
        }}
        appear
        onEnter={handleOnEnter}
        onExited={handleOnExited}
      >
        <div className={className} tabIndex={-1} role="dialog" aria-modal={true}>
          <div className="modal-dialog" role="document">
            {renderContent(children)}
          </div>
        </div>
      </CSSTransition>
      <CSSTransition
        in={isOpen}
        mountOnEnter
        unmountOnExit
        timeout={300}
        appear
        classNames={{
          appear: 'show',
          appearActive: 'show',
          enter: 'show',
          enterActive: 'show',
          enterDone: 'show',
        }}
      >
        <div className="modal-backdrop fade" />
      </CSSTransition>
    </Portal>
  );
}

Modal.defaultProps = defaultProps;
