import cn from "classnames";
import { FieldProps } from "formik";
import { createPropsGetter, InputType } from "./utils";

type Props<V = any> = {
  className?: string;
  label?: string;
} & React.InputHTMLAttributes<HTMLInputElement> & FieldProps<V> & Partial<DefaultProps>;

type DefaultProps = Readonly<typeof defaultProps>;

const defaultProps = Object.freeze({
  type: 'text' as InputType,
});

const getProps = createPropsGetter(defaultProps);

export const Input = (props: Props) => {
  const {
    field: { name, ...field },
    form: { touched, errors },
    className,
    label,
    type,
    ...others
  } = getProps(props);

  const error = errors[name];
  const touch = touched[name];

  return (
    <div className={cn('form-group', { 'animated shake error': !!error }, className)}>
      {
        label && (
          <label htmlFor={name}>{ label }</label>
        )
      }
      <input
        name={name}
        className="form-control"
        type={type}
        {...field}
        {...others}
      />
      {
        touch && error && (
          <span className="text-danger">{error}</span>
        )
      }
    </div>
  );
}

Input.defaultProps = defaultProps;
