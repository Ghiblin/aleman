import { Children, useState, useEffect, ReactNode, useRef, Fragment } from "react";
import cn from 'classnames';
import { createPropsGetter, keyCodes } from "./utils";
import { isObject } from "util";

type Props = {
  text: string;
  imageURL: string;
  className?: string;
  children: ReactNode | NamedChildrenSlots[];
} & Partial<DefaultProps>;

type NamedChildrenSlots = {
  header?: string;
  list: ReactNode;
}

type DefaultProps = Readonly<typeof defaultProps>;

const defaultProps = {
  disabled: false,
}

const isNamedSlots = (children: any): children is NamedChildrenSlots =>
  isObject(children) && 'list' in children;

const getProps = createPropsGetter(defaultProps);

export const Megamenu = (props: Props) => {
  const { text, imageURL, className, children, disabled } = getProps(props);

  const container = useRef<HTMLLIElement>(null);
  const [show, setShow] = useState(false);
  const toggle = (e: Event) => {
    if (disabled) {
      return e.preventDefault();
    }
    setShow(!show);
  };
  useEffect(() => {
    const eventsName: Array<'click' | 'touchstart' | 'keyup'> = ['click', 'touchstart', 'keyup'];
    function handleDocumentClick(e: MouseEvent | KeyboardEvent | TouchEvent) {
      if ((e as KeyboardEvent).which === 3 || (e.type === 'keyup' && (e as KeyboardEvent).which === keyCodes.tab)) {
        return;
      }
      if (!container.current) return;
  
      if (e.target instanceof Element) {
        if (container.current.contains(e.target) 
          && container.current !== e.target 
          && (e.type !== 'keyup' || (e as MouseEvent).which === keyCodes.tab)) {
          return;
        }
      }
  
      toggle(e);
    }
    if (show) {
      eventsName.forEach((eventName) => {
        document.addEventListener(eventName, handleDocumentClick, true)
      });
    }
    return function() {
      eventsName.forEach((eventName) => {
        document.removeEventListener(eventName, handleDocumentClick, true);
      });
    }
  });

  function renderItem(children: ReactNode | NamedChildrenSlots, key: number) {
    if (!children) {
      throw new Error('Children is mandatory');
    }

    if (isNamedSlots(children)) {
      return (
        <Fragment key={key}>
          {children.header && <h5 key={children.header}>{ children.header }</h5>}
          <ul className="list-unstyled mb-3">
            {
              Children.map(children.list, (child, i) => (
                <li key={i} className="nav-item">{ child }</li>
              )) 
            }
          </ul>
        </Fragment>
      );
    }

    return (
      <ul key={key} className="list-unstyled mb-3">
        {
          Children.map(children, (child, i) => (
            <li key={i} className="nav-item">{ child }</li>
          ))
        }
      </ul>
    );
  }

  return (
    <li
      ref={container}
      className={cn('nav-item', 'dropdown', 'menu-large', className, show)}
    >
      <a href="#"
        className="dropdown-toggle"
        aria-expanded={show}
        onClick={(e) => { 
          e.stopPropagation(); 
          toggle((e as unknown) as Event); 
        }}
      >
        {text}
        <b className="caret" />
      </a>
      <ul className={cn('dropdown-menu', 'megamenu', show)} 
          style={{ display: show ? 'block' : 'none'}}
      >
        <li>
          <div className="row">
            <div className="col-lg-6">
              <img src={imageURL} className="img-fluid d-none d-lg-block" />
            </div>
            <div className="col-lg-6 col-md-12" style={{ columns: 2 }}>
              { (Array.isArray(children) ? children : [children]).map(renderItem) }
            </div>
          </div>
        </li>
      </ul>
    </li>
  );
}