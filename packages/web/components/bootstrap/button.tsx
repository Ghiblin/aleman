import { MouseEvent, Component } from 'react';
import { withDefaultProps } from './utils';

type Props = {
  onClick: (e: MouseEvent<HTMLElement>) => void;
} & DefaultProps;

type DefaultProps = Readonly<typeof defaultProps>;

const defaultProps = Object.freeze({
  color: 'blue' as 'blue' | 'green' | 'red',
  type: 'button' as 'button' | 'submit',
});

const resolveColorTheme = (color: DefaultProps['color']) => {
  const btnThemes = {
    blue: 'btn-primary',
    green: 'btn-secondary',
    red: 'btn-accent',
  }
  return btnThemes[color] || 'btn-default';
}

export const Button = withDefaultProps(
  defaultProps,
  class extends Component<Props> {
    render() {
      const { onClick: handleClick, color, type, children } = this.props;
      const cssClass = resolveColorTheme(color);
      return (
        <button type={type} className={cssClass} onClick={handleClick}>
          {children}
        </button>
      )
    }
  } 
)


