import { Component, ReactNode, useState } from 'react';
import cn from 'classnames';
import { createPropsGetter, isObject } from './utils';
import Transition, { ENTERED, ENTERING, EXITING } from 'react-transition-group/Transition';

type Props = {
  children: ReactNode | NamedChildrenSlots[];
} & Partial<DefaultProps>;
type DefaultProps = Readonly<typeof defaultProps>;
type State = Readonly<typeof initialState>;

type NamedChildrenSlots = {
  content: ReactNode;
  caption?: ReactNode;
}

const initialState = Object.freeze({
  current: 0,
  direction: 'right' as 'right' | 'left',
  transitioning: false,
});

const defaultProps = Object.freeze({
  indicators: false,
  fade: false,
  interval: 50,
});

const getProps = createPropsGetter(defaultProps);
const isNamedSlots = (children: any): children is NamedChildrenSlots =>
  isObject(children) && 'content' in children;

export class Carousel extends Component<Props, State> {
  static defaultProps = defaultProps;
  state = initialState;
  _interval?: NodeJS.Timeout;

  componentDidMount() {
    const { interval } = getProps(this.props);
    if (interval) {
      this._interval = setInterval(this._next, interval * 1000);
    }
  }

  componentWillUnmount() {
    if (this._interval) {
      clearInterval(this._interval);
    }
  }

  render() {
    const { children, indicators, fade } = getProps(this.props);
    const { current, direction } = this.state;

    if (!children) {
      throw new Error('children is mandatory!');
    }

    return (
      <div className={cn('carousel', 'slide', { 'carousel-fade': fade })}>
        {
          indicators && <ol className="carousel-indicators">
            {
              (Array.isArray(children) ? children : [children]).map((_child, index) => (
                <li key={index} className={cn({ active: index === current })} />
              ))
            }
          </ol>
        }
        <div className="carousel-inner">
          {
            (Array.isArray(children) ? children : [children]).map((child, index) => (
              <CarouselItem
                key={index} 
                active={current === index}
                slide={true}
                direction={direction}
                onExiting={this._onExiting}
                onExited={this._onExited}
              >
                { this._renderItem(child) }
              </CarouselItem>
            ))
          }
          <a className="carousel-control-prev" role="button" onClick={this._prev}>
            <span className="carousel-control-prev-icon" aria-hidden="true" />
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" role="button" onClick={this._next}>
            <span className="carousel-control-next-icon" aria-hidden="true" />
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>
    )
  }

  private _renderItem = (child: ReactNode | NamedChildrenSlots) => {
    if (isNamedSlots(child)) {
      return (
        <>
          { child.content }
          { child.caption && (
            <div className="carousel-caption d-none d-md-block">
              { child.caption }
            </div>
          )}
        </>
      );
    }

    return (child);
  }

  private _next = () => {
    if (this.state.transitioning) return;
    const { children } = this.props;

    // setActiveIndex(activeIndex === items.length - 1 ? 0 : activeIndex + 1);
    this.setState(({ current })  => ({
      transitioning: true,
      direction: 'right',
      current: current === (Array.isArray(children) ? children : [children]).length - 1 ? 0 : current + 1,
    }));
  }

  private _prev = () => {
    if (this.state.transitioning) return;
    const { children } = this.props;

    this.setState(({ current }) => ({
      transitioning: true,
      direction: 'left',
      current: current === 0 ? (Array.isArray(children) ? children : [children]).length - 1 : current - 1,
    }));
  }

  private _onExiting = () => {
    this.setState({ transitioning: true });
  }

  private _onExited = () => {
    this.setState({ transitioning: false });
  }
}

type ItemProps = {
  active: boolean;
  direction: 'right' | 'left';
  slide: boolean;
  children: ReactNode;
  onEnter?: (node: HTMLElement, isAppearing: boolean) => void;
  onEntering?: (node: HTMLElement, isAppearing: boolean) => void;
  onEntered?: (node: HTMLElement, isAppearing: boolean) => void;
  onExit?: (node: HTMLElement) => void;
  onExiting?: (node: HTMLElement) => void;
  onExited?: (node: HTMLElement) => void;
}

const CarouselItem = ({ active, direction, slide, children, ...props }: ItemProps) => {
  const [startAnimation, setStartAnimation] = useState(false);

  function onEnter(node: HTMLElement, isAppearing: boolean) {
    setStartAnimation(false);
    props.onEnter && props.onEnter(node, isAppearing);
  }

  function onEntering(node: HTMLElement, isAppearing: boolean) {
    // getting this variable triggers a reflow
    node.offsetHeight;
    setStartAnimation(true);
    props.onEntering && props.onEntering(node, isAppearing);
  }

  function onEntered(node: HTMLElement, isAppearing: boolean) {
    props.onEntered && props.onEntered(node, isAppearing);
  }

  function onExit(node: HTMLElement) {
    setStartAnimation(false);
    props.onExit && props.onExit(node);
  }

  function onExiting(node: HTMLElement) {
    setStartAnimation(true);
    props.onExiting && props.onExiting(node);
  }

  function onExited(node: HTMLElement) {
    props.onExited && props.onExited(node);
  }
  return (
    <Transition
      in={active}
      enter={slide}
      exit={slide}
      timeout={500}
      onEnter={onEnter}
      onEntering={onEntering}
      onEntered={onEntered}
      onExit={onExit}
      onExiting={onExiting}
      onExited={onExited}
    >
      {
        (status) => {
          const active = status === ENTERED || status === EXITING;
          const directionCN = (status === ENTERING || status === EXITING) && startAnimation &&
            (direction === 'right' ? 'carousel-item-left' : 'carousel-item-right');
          const orderCN = (status === ENTERING) &&
            (direction === 'right' ? 'carousel-item-next' : 'carousel-item-prev');
          return (
            <div className={cn('carousel-item', directionCN, orderCN, { active })}>
              { children }
            </div>
          )
        }
      }
    </Transition>
  )
}