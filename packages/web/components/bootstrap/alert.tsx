import { ReactNode, useState } from "react";
import cn from "classnames";
import { Color, createPropsGetter } from "./utils";
import { CSSTransition } from "react-transition-group";

type Props = {
  children: ReactNode;
} & Partial<DefaultProps>

type DefaultProps = Readonly<typeof defaultProps>;

const defaultProps = Object.freeze({
  color: 'light' as Color,
  dismissable: false,
  fade: false,
});

const getProps = createPropsGetter(defaultProps);

export const Alert = (props: Props) => {
  const [isVisible, setVisible] = useState(true);
  const hide = () => setVisible(false);

  const { color, dismissable, fade, children } = getProps(props);
  return (
    <CSSTransition
      in={isVisible}
      classNames={{
        exit: cn('show', { fade }),
      }}
      onExiting={(node) => { node.classList.remove('show'); }}
      timeout={500}
      unmountOnExit
    >
      <div className={cn('alert', `alert-${color}`, { 'alert-dismissible': dismissable })} role="alert">
        {
          dismissable && (
            <button type="button" className="close" aria-label="Close" onClick={hide}>
              <span aria-hidden="true">&times;</span>
            </button>
          )
        }
        { children }
      </div>
    </CSSTransition>
  )
}

Alert.defaultProps = defaultProps;
