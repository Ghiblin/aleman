import { Component, ReactNode } from 'react';
import { hasRender, hasChildren } from './utils';

type Props = {
  onToggle(on: boolean): void;
} & RenderProps;

type RenderProps =
  | { children: (api: API) => ReactNode; }
  | { render: (api: API) => ReactNode; }

type API = ReturnType<Toggle['getApi']>;

type State = Readonly<typeof initialState>
const initialState = {
  on: false,
};

export class Toggle extends Component<Props, State> {
  readonly state = initialState;

  private toggle = () => 
    this.setState(
      ({ on }) => ({ on: !on}),
      () => this.props.onToggle(this.state.on)
    )
  
  private getApi() {
    return {
      on: this.state.on,
      toggle: this.toggle,
    };
  }

  public render() {
    if (hasRender(this.props)) {
      return this.props.render(this.getApi());
    }

    if (hasChildren(this.props)) {
      return this.props.children(this.getApi());
    }

    throw new Error('one of children or render is mandatory and needs to be a function!');
  }
}
