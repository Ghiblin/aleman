import cn from "classnames";
import { FieldProps } from "formik";
import { createPropsGetter } from "./utils";

type Props<V = any> = {
  className?: string;
  label?: string;
} & React.InputHTMLAttributes<HTMLTextAreaElement> & FieldProps<V> & Partial<DefaultProps>;

type DefaultProps = Readonly<typeof defaultProps>;

const defaultProps = Object.freeze({
  rows: 3,
});

const getProps = createPropsGetter(defaultProps);

export const Textarea = (props: Props) => {
  const {
    field: { name, ...field },
    form: { touched, errors },
    className,
    label,
    ...others
  } = getProps(props);

  const error = errors[name];
  const touch = touched[name];

  return (
    <div className={cn('form-group', { 'animated shake error': !!error }, className)}>
      {
        label && (
          <label htmlFor={name}>{ label }</label>
        )
      }
      <textarea
        name={name}
        className="form-control"
        {...field}
        {...others}
      />
      {
        touch && error && (
          <span className="text-danger">{error}</span>
        )
      }
    </div>
  );
}

Textarea.defaultProps = defaultProps;
