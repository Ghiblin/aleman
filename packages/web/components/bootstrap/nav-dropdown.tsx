import { Children, ReactNode, Component } from 'react';
import cn from 'classnames';
import { keyCodes, createPropsGetter } from './utils';

type Props = {
  text: string;
  className?: string;
  children: ReactNode | ReactNode[];
} & Partial<DefaultProps>;

type DefaultProps = Readonly<typeof defaultProps>;
type State = Readonly<typeof initialState>;

const defaultProps = Object.freeze({
  disabled: false,
});

const initialState = Object.freeze({
  show: false,
});

const getProps = createPropsGetter(defaultProps);
const eventsName: Array<'click' | 'touchstart' | 'keyup'> = ['click', 'touchstart', 'keyup'];

export class NavDropdown extends Component<Props, State> {
  readonly state = initialState;
  static defaultProps = defaultProps;
  
  _$container: HTMLLIElement | null = null;

  componentDidUpdate(_prevProps: Props, prevState: State) {
    if (prevState.show !== this.state.show) {
      if (this.state.show) {
        this._addEvents();
      } else {
        this._removeEvents();
      }
    }
  }

  componentWillUnmount() {
    this._removeEvents();
  }

  _toggle = (e: Event) => {    
    if (getProps(this.props).disabled) {
      return e && e.preventDefault();
    }

    this.setState(({ show }) => ({ show: !show }));
  }

  render() {
    const { children, className, text } = getProps(this.props);
    const { show } = this.state;

    return (
      <li 
        ref={(el) => { this._$container = el; }}
        className={cn('nav-item', 'dropdown', className, show)} 
      >
        <a href="#"
          className="dropdown-toggle nav-link" 
          aria-expanded={show} 
          onClick={(e) => this._toggle((e as unknown) as Event)}
        >
          {text}
          <b className="caret" />
        </a>
        <ul className={cn('dropdown-menu', show)} style={{display: show ? 'block' : ''}}>
          {
            Children.map(children, (child, i) => (
              <li className="dropdown-item" key={i} onClick={(e) => this._toggle((e as unknown) as Event)}>
                { child }
              </li>
            ))
          }
        </ul>
      </li>
    );
  }

  private _addEvents = () => {
    eventsName.forEach((name) => {
      document.addEventListener(name, this._handleDocumentClick, true);
    });
  }

  private _removeEvents = () => {
    eventsName.forEach((name) => {
      document.removeEventListener(name, this._handleDocumentClick, true);
    });
  }

  private _handleDocumentClick = (e: MouseEvent | KeyboardEvent | TouchEvent) => {
    if ((e as KeyboardEvent).which === 3 || (e.type === 'keyup' && (e as KeyboardEvent).which === keyCodes.tab)) {
      return;
    }
    if (!this._$container) return;

    if (e.target instanceof Element) {
      if (this._$container.contains(e.target) 
       && this._$container !== e.target 
       && (e.type !== 'keyup' || (e as MouseEvent).which === keyCodes.tab)) {
        return;
      }
    }

    this._toggle(e);
  }
}
