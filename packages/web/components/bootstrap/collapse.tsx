import { useState, ReactNode } from 'react';
import cn from 'classnames';
import { createPropsGetter } from './utils';
import { CSSTransition } from 'react-transition-group';

type Props = {
  className?: string;
  children: ReactNode | ReactNode[];  
} & Partial<DefalutProps>;

type DefalutProps = Readonly<typeof defaultProps>;

const defaultProps = {
  nav: false,
};

const ClassName = {
  SHOW: 'show',
  COLLAPSE: 'collapse',
  COLLAPSING: 'collapsing',
  COLLAPSED: 'collapsed'
}
const reflow = (element: HTMLElement) => element.offsetHeight;
const getProps = createPropsGetter(defaultProps);

export const Collapse = (props: Props) => {
  const [collapsed, setCollapsed] = useState(true);
  const collapse = () => setCollapsed(true);
  const toggle = () => setCollapsed(!collapsed);
  const { className, nav, children } = getProps(props);

  return (
    <>
      <button
        type="button"
        onClick={toggle}
        className={cn(className, { 'navbar-toggler': nav,  collapsed })}
        aria-expanded={collapsed}
      >
        <span className="sr-only">Toggle navigation</span>
        <i className="fa fa-align-justify" />
      </button>
      <CSSTransition
        in={!collapsed}
        classNames={{
          enter: ClassName.COLLAPSING,
          enterActive: ClassName.COLLAPSING,
          enterDone: [ClassName.COLLAPSE, ClassName.SHOW].join(' '),
          exit: [ClassName.COLLAPSE, ClassName.SHOW].join(' '),
          exitActive: ClassName.COLLAPSING,
          exitDone: ClassName.COLLAPSE,
        }}
        timeout={300}
        onEnter={(node) => { 
          node.style.height = '0px';
        }}
        onEntering={(node) => { 
          node.style.height = `${node.scrollHeight}px`;
        }}
        onEntered={(node) => { 
          node.style.height = '';
        }}
        onExit={(node) => {
          node.style.height = `${node.getBoundingClientRect().height}px`;
          reflow(node);
          node.classList.remove(ClassName.COLLAPSE);
          node.classList.remove(ClassName.SHOW);
          node.classList.add(ClassName.COLLAPSING);
        }}
        onExiting={(node) => {
          node.style.height = ``;
        }}
      >  
        <div
          className={cn('collapse', { 'navbar-collapse': nav })}
          onClick={collapse}
        >
          { children }
        </div>
      </CSSTransition>
    </>
  );
}

Collapse.defaultProps = defaultProps;
