import React, { InputHTMLAttributes } from 'react';
import cn from 'classnames';

interface TextareaProps {
  label?: string;
  name: string;
  value?: string;
  errors: (string | undefined)[];
  setErrors(errors: (string | undefined)[]): void;
  pristine: boolean;
  onChange(e: React.ChangeEvent<HTMLTextAreaElement>): void;
  validate(): Promise<boolean>;
  validating: boolean;
  formSubmitted: boolean;
}

export const Textarea: React.FC<TextareaProps & InputHTMLAttributes<HTMLTextAreaElement>> = ({
  label,
  name,
  value,
  onChange,
  errors,
  setErrors,
  pristine,
  validating,
  validate,
  formSubmitted,
  ...other
}) => {
  const showErrors = (!pristine || formSubmitted) && errors.length > 0;
  const isValid = (!pristine || formSubmitted) && !errors.length;
  return (
    <div className="form-group">
      {label && <label htmlFor={name}>{label}</label>}
      <textarea
        className={cn('form-control', {'is-valid': isValid, 'is-invalid': showErrors })} 
        name={name} 
        value={value} 
        onChange={onChange}
        onBlur={() => !pristine && validate()}
        {...other}
      />
      { showErrors && <div className="invalid-feedback">{errors[0]}</div>}
    </div>
  );
};
