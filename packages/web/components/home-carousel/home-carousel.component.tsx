import { Carousel } from '../bootstrap/carousel';

const items = [
  {
    src: "studio.jpeg",
    altText: "Studio",
    caption: "Studio",
    textColor: "primary",
  },
  {
    src: "centro-prelievi.jpeg",
    altText: "Centro prelievi",
    caption: "Centro prelievi",
  },
  {
    src: "family.jpeg",
    altText: "Famiglia",
    caption: "Famiglia",
    textColor: "primary",
  },
  {
    src: "gravidanza.jpeg",
    altText: "Gravidanza",
    caption: "Gravidanza",
  },
];

type Props = {
  interval?: number;
  children?: never;
};

export const HomeCarousel = ({ interval }: Props) => (
  <Carousel interval={interval} indicators >
  {
    items.map((item) => ({
      content: (
        <div key={item.src}
          style={{
            height: '80vh',
            backgroundImage: `url(/static/images/home-carousel/${item.src})`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
          }}
        />),
      caption: (
        <h5 className={item.textColor && `text-${item.textColor}`}>{item.caption}</h5>
      ),
    }))
  }
  </Carousel>
);
