import { Component } from 'react';
import { createPortal } from 'react-dom';

export interface PortalProps {
  selector: string;
}

export class Portal extends Component<PortalProps> {
  private element: Element | null = null;

  public componentDidMount() {
    this.element = document.querySelector(this.props.selector);
  }

  public render() {
    if (!this.element) {
      return null;
    }

    return createPortal(this.props.children, this.element);
  }
}