import React from 'react';

import { TopBar } from './top-bar/top-bar.component';
import { Header } from './header/header.component';

import '../styles/styles.scss';
import { Footer } from './footer/footer.component';

interface LayoutProps {
  currentUser?: string;
  isAuthenticated: boolean;
}

export const Layout: React.SFC<LayoutProps> = ({ isAuthenticated, children }) => (
  <>
    <TopBar isAuthenticated={isAuthenticated} />
    <Header />
    {children}
    <Footer />
  </>
);
