import Link from "next/link";

export const Footer = () => (
  <footer className="main-footer">
    <div className="container">
      <div className="row">
        <div className="col-lg-8">
          <h4 className="h6">Chi siamo</h4>
          <p>
            Il <strong>Poliambulatorio Medico Specialistico Aleman</strong> offre
            tecnologie avanzate in grado di fornire una vasta gamma di servizi
            specialistici per la vostra salute e benessere.<br/>
            Dal 2009 siamo il vostro poliambulatorio in Valle Seriana.
          </p>
          <hr />
          <h4 className="h6">Join out Newsletter</h4>
          <form>
            <div className="input-group">
              <input className="form-control" />
              <div className="input-group-append">
                <button type="submit" className="btn btn-secondary">
                  <i className="fas fa-paper-plane" />
                </button>
              </div>
            </div>
          </form>
          <hr className="d-block d-lg-none" />
        </div>
        <div className="col-lg-4">
          <h4 className="h6">Contatti</h4>
          <p className="text-uppercase">
            <strong>Poliambulatorio Aleman</strong><br />
            Via Peppino Ribolla 1<br />
            Alzano Lombardo (BG)
          </p>
          <Link href="/contact-us" passHref>
            <a className="btn btn-template-main">Vai ai contatti</a>
          </Link>
        </div>
      </div>
    </div>
    <div className="copyrights">
      <div className="container">
        <div className="row">
          <div className="col-lg-8 text-center-md">
            <p>@2019. Poliambulatorio Aleman - P. iva 03471080162</p>
          </div>
          <div className="col-lg-4 text-center-md">
            Template design by <a href="mailto:alessandro.vitali@gmail.com">
              Alessandro Vitali
            </a>
          </div>
        </div>
      </div>
    </div>
  </footer>
)