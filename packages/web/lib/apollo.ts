import { HttpLink } from 'apollo-link-http';
import { withData } from './withData';
import getConfig from 'next/config';

const {
  publicRuntimeConfig: {
    API_URL,
  }
} = getConfig();

const config = {
  link: new HttpLink({
    uri: `${API_URL}graphql`,
  }),
};
export const withApollo = withData(config);