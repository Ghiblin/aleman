import React from 'react';
import { ApolloConfig, initApollo } from './initApollo';
import { NextContext, NextComponentType } from 'next';
import { getDataFromTree, ApolloProvider } from 'react-apollo';
import Head from 'next/head';
import ApolloClient from 'apollo-client';
import { NormalizedCacheObject } from 'apollo-cache-inmemory';

function getComponentDisplayName<T>(Component: NextComponentType<T>) {
  return (Component as any).displayName || (Component as any).name || 'Unknown';
}

export interface ComposedComponentProps {
  url?: {
    query: Record<string, string | string[] | undefined>;
    pathname: string;
  }
  ctx?: NextContext;
}

export interface WithDataProps {
  serverState: {
    apollo: any;
  }
}

export const withData = (apolloConfig: ApolloConfig) =>
  (ComposedComponent: NextComponentType<ComposedComponentProps>) => {
    return class WithData extends React.Component<WithDataProps> {
      static diplayName = `WithData(${getComponentDisplayName<ComposedComponentProps>(ComposedComponent)})`;

      static async getInitialProps(ctx: NextContext) {
        let serverState = { apollo: {} };

        // Evaluate the composed component's getInitialProps()
        let composedInitialProps = {};
        if (ComposedComponent.getInitialProps) {
          composedInitialProps = await ComposedComponent.getInitialProps(ctx);
        }

        // Run all GraphQL queries in the component tree
        // and extract the resulting data
        const apollo = initApollo(apolloConfig, null, ctx);

        try {
          // Provide the `url` prop data in case a GraphQL query uses it
          const url = {
            query: ctx.query,
            pathname: ctx.pathname,
          };
          
          // Run all GraphQL queries
          await getDataFromTree(
            <ApolloProvider client={apollo}>
              <ComposedComponent
                url={url}
                ctx={ctx}
                {...composedInitialProps}
              />
            </ApolloProvider>,
            {
              router: {
                asPath: ctx.asPath,
                pathname: ctx.pathname,
                query: ctx.query,
              }
            }
          );
        } catch (error) {
          // Prevent Apollo Client GraphQL errors from crashing SSR.
          // Handle them in components via the data.error prop:
          // http://dev.apollodata.com/react/api-queries.html#graphql-query-data-error
          console.log(error);
        }

        if (!process.browser) {
          // getDataFromTree does not call componentWillUnmount
          // head side effect therefore need to be cleared manually
          Head.rewind();
        }

        // Extract query data from the Apollo store
        serverState = {
          apollo: {
            data: apollo.cache.extract(),
          },
        };

        return {
          serverState,
          ...composedInitialProps,
        };
      }

      readonly apollo: ApolloClient<NormalizedCacheObject>;

      constructor(props: Readonly<WithDataProps>) {
        super(props);
        this.apollo = initApollo(
          apolloConfig,
          this.props.serverState.apollo.data,
        );
      }

      public render() {
        return (
          <ApolloProvider client={this.apollo}>
            <ComposedComponent {...this.props} />
          </ApolloProvider>
        );
      }
    }
  }