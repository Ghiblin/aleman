import Strapi from 'strapi-sdk-javascript';
import Cookies from 'js-cookie';
import jwtDecode from "jwt-decode";
import getConfig from 'next/config';
import { Authentication } from 'strapi-sdk-javascript/build/main/lib/sdk';
import Router from 'next/router';
import { IncomingMessage } from 'http';

const {
  publicRuntimeConfig: {
    API_URL,
  }
} = getConfig();
const strapi = new Strapi(API_URL);

export const strapiRegister = async (username: string, email: string, password: string) => {
  if (!process.browser) {
    return;
  }

  const auth = await strapi.register(username, email, password);
  setToken(auth);
  return auth;
}

export const strapiLogin = async (email: string, password: string) => {
  if (!process.browser) {
    return;
  }

  // Get a token
  const auth = await strapi.login(email, password);
  setToken(auth);
  return auth;
}

export const setToken = (token: Authentication) => {
  if (!process.browser) {
    return;
  }
  Cookies.set('username', (token.user as any).username);
  Cookies.set('jwt', token.jwt);

  if (Cookies.get('username')) {
    Router.push('/');
  }
}

export const unsetToken = () => {
  if (!process.browser) {
    return;
  }

  Cookies.remove('jwt');
  Cookies.remove('username');
  Cookies.remove('cart');

  // to support logging out from all windows
  window.localStorage.setItem("logout", Date.now().toString());
  Router.push("/");
}

export const getUserFromServerCookie = (req: IncomingMessage) => {
  if (!req.headers.cookie || '') {
    return;
  }

  let username = req.headers.cookie
    .split(';')
    .find((c) => c.trim().startsWith('username='));
  if (username) {
    username = username.split('=')[1];
  }

  const jwtCookie = req.headers.cookie
    .split(";")
    .find(c => c.trim().startsWith("jwt="));
  if (!jwtCookie) {
    return undefined;
  }
  const jwt = jwtCookie.split("=")[1];
  return jwtDecode(jwt), username;
}

export const getUserFromLocalCookie = () => {
  return Cookies.get('username');
}