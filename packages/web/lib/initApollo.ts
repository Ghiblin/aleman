import { ApolloClient, ApolloClientOptions } from 'apollo-client';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import fetch from 'isomorphic-unfetch';
import { ApolloCache } from 'apollo-cache';
import { NextContext } from 'next';
import { ApolloLink } from 'apollo-link';

let apolloClient: ApolloClient<{}> | null = null;

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
  (global as any).fetch = fetch;
}

const createDefaultCache = () => new InMemoryCache();

export interface ApolloConfig {
  link?: ApolloLink;
  // cache?: ApolloCache<NormalizedCacheObject>;
  // ssrForceFetchDelay?: number;
  // ssrMode?: boolean;
  // connectToDevTools?: boolean;
  // queryDeduplication?: boolean;
  createCache?: () => ApolloCache<NormalizedCacheObject>;
}

function create(apolloConfig: ApolloConfig, initialState: any) {
  const createCache = apolloConfig.createCache || createDefaultCache;
  const cache = createCache().restore(initialState || {});
  delete apolloConfig.createCache;

  const config: ApolloClientOptions<NormalizedCacheObject> = {
    connectToDevTools: (process as any).browser,
    ssrMode: !(process as any).browser, // Disables forceFetch on the server (so queries are only run once)
    cache,
    ...apolloConfig,
  };

  return new ApolloClient(config);
}

export function initApollo(
  apolloConfig: ApolloConfig | ((ctx: any) => ApolloConfig), 
  initialState: any | null, 
  ctx?: NextContext
) {
  if (typeof apolloConfig === 'function') {
    apolloConfig = apolloConfig(ctx);
  }
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!(process as any).browser) {
    return create(apolloConfig, initialState || {});
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create(apolloConfig, initialState || {});
  }

  return apolloClient;
}