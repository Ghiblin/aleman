import Router, { UrlLike } from 'next/router';
import { format, resolve, parse } from 'url';
import { NextComponentType, NextContext } from 'next';
import { getDataFromTree } from 'react-apollo';
import { ComposedComponentProps } from './withData';

export const prefetch = async (href: string | UrlLike) => {
  // if we're running server side do nothing
  if (!process.browser) return;

  const url = typeof href !== 'string' ? format(href) : href;
  const { pathname } = window.location;
  const parsedHref = resolve(pathname, url);
  const { query } = typeof href !== 'string' ? href : parse(url, true);
  const Component = await Router.prefetch(parsedHref) as NextComponentType<ComposedComponentProps>;

  // if Component exists and has getInitialProps
  // fetch the component props (the component should save it in cache)
  if (Component && Component.getInitialProps) {
    const ctx: NextContext = {
      pathname,
      query: (query as Record<string, string | string[] | undefined>),
      asPath: pathname,
    };
    // await Component.getInitialProps(ctx);
    const composedInitialProps = await Component.getInitialProps(ctx);
    await getDataFromTree(<Component ctx={ctx} {...composedInitialProps} />, {
      router: {
        asPath: ctx.asPath,
        pathname: ctx.pathname,
        query: ctx.query
      },
    });
  }
}