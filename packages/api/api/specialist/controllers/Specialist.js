'use strict';

/**
 * Specialist.js controller
 *
 * @description: A set of functions called "actions" for managing `Specialist`.
 */

module.exports = {

  /**
   * Retrieve specialist records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.specialist.search(ctx.query);
    } else {
      return strapi.services.specialist.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a specialist record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.specialist.fetch(ctx.params);
  },

  /**
   * Count specialist records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.specialist.count(ctx.query);
  },

  /**
   * Create a/an specialist record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.specialist.add(ctx.request.body);
  },

  /**
   * Update a/an specialist record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.specialist.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an specialist record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.specialist.remove(ctx.params);
  }
};
